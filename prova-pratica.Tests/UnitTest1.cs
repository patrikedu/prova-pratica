using NUnit.Framework;
using ProvaPratica;

namespace prova_pratica.Tests
{
    public class Tests
    {
       
        [Test]
        public void TesteDeCpf()
        {
            string documento = "05470265013";
            bool result = true;
            
            Assert.AreEqual(result, Validador.CPF(documento));
        }

        public void TesteDeCnpj()
        {
            string documento = "97311742000170";
            bool result = true;
            
            Assert.AreEqual(result, Validador.CNPJ(documento));
        }

          
    }
}